# Wercker example

An example using [Wercker](https://wercker.com) for continuous deployment to [Surge.sh](https://surge.sh).

## License

[The MIT License (MIT)](LICENSE.md)

Copyright © 2015 [Chloi Inc.](http://chloi.io)
